use super::Element;

impl Element {
    /// Returns the name of the element in English.
    ///
    /// ```
    /// use mendeleev::Element;
    /// assert_eq!(Element::H.name(), "Hydrogen");
    /// ```
    pub const fn name(&self) -> &'static str {
        match self {
            Element::H => "Hydrogen",
            Element::He => "Helium",
            Element::Li => "Lithium",
            Element::Be => "Beryllium",
            Element::B => "Boron",
            Element::C => "Carbon",
            Element::N => "Nitrogen",
            Element::O => "Oxygen",
            Element::F => "Fluorine",
            Element::Ne => "Neon",
            Element::Na => "Sodium",
            Element::Mg => "Magnesium",
            Element::Al => "Aluminum",
            Element::Si => "Silicon",
            Element::P => "Phosphorus",
            Element::S => "Sulfur",
            Element::Cl => "Chlorine",
            Element::Ar => "Argon",
            Element::K => "Potassium",
            Element::Ca => "Calcium",
            Element::Sc => "Scandium",
            Element::Ti => "Titanium",
            Element::V => "Vanadium",
            Element::Cr => "Chromium",
            Element::Mn => "Manganese",
            Element::Fe => "Iron",
            Element::Co => "Cobalt",
            Element::Ni => "Nickel",
            Element::Cu => "Copper",
            Element::Zn => "Zinc",
            Element::Ga => "Gallium",
            Element::Ge => "Germanium",
            Element::As => "Arsenic",
            Element::Se => "Selenium",
            Element::Br => "Bromine",
            Element::Kr => "Krypton",
            Element::Rb => "Rubidium",
            Element::Sr => "Strontium",
            Element::Y => "Yttrium",
            Element::Zr => "Zirconium",
            Element::Nb => "Niobium",
            Element::Mo => "Molybdenum",
            Element::Tc => "Technetium",
            Element::Ru => "Ruthenium",
            Element::Rh => "Rhodium",
            Element::Pd => "Palladium",
            Element::Ag => "Silver",
            Element::Cd => "Cadmium",
            Element::In => "Indium",
            Element::Sn => "Tin",
            Element::Sb => "Antimony",
            Element::Te => "Tellurium",
            Element::I => "Iodine",
            Element::Xe => "Xenon",
            Element::Cs => "Cesium",
            Element::Ba => "Barium",
            Element::La => "Lanthanum",
            Element::Ce => "Cerium",
            Element::Pr => "Praseodymium",
            Element::Nd => "Neodymium",
            Element::Pm => "Promethium",
            Element::Sm => "Samarium",
            Element::Eu => "Europium",
            Element::Gd => "Gadolinium",
            Element::Tb => "Terbium",
            Element::Dy => "Dysprosium",
            Element::Ho => "Holmium",
            Element::Er => "Erbium",
            Element::Tm => "Thulium",
            Element::Yb => "Ytterbium",
            Element::Lu => "Lutetium",
            Element::Hf => "Hafnium",
            Element::Ta => "Tantalum",
            Element::W => "Tungsten",
            Element::Re => "Rhenium",
            Element::Os => "Osmium",
            Element::Ir => "Iridium",
            Element::Pt => "Platinum",
            Element::Au => "Gold",
            Element::Hg => "Mercury",
            Element::Tl => "Thallium",
            Element::Pb => "Lead",
            Element::Bi => "Bismuth",
            Element::Po => "Polonium",
            Element::At => "Astatine",
            Element::Rn => "Radon",
            Element::Fr => "Francium",
            Element::Ra => "Radium",
            Element::Ac => "Actinium",
            Element::Th => "Thorium",
            Element::Pa => "Protactinium",
            Element::U => "Uranium",
            Element::Np => "Neptunium",
            Element::Pu => "Plutonium",
            Element::Am => "Americium",
            Element::Cm => "Curium",
            Element::Bk => "Berkelium",
            Element::Cf => "Californium",
            Element::Es => "Einsteinium",
            Element::Fm => "Fermium",
            Element::Md => "Mendelevium",
            Element::No => "Nobelium",
            Element::Lr => "Lawrencium",
            Element::Rf => "Rutherfordium",
            Element::Db => "Dubnium",
            Element::Sg => "Seaborgium",
            Element::Bh => "Bohrium",
            Element::Hs => "Hassium",
            Element::Mt => "Meitnerium",
            Element::Ds => "Darmstadtium",
            Element::Rg => "Roentgenium",
            Element::Cn => "Copernicium",
            Element::Nh => "Nihonium",
            Element::Fl => "Flerovium",
            Element::Mc => "Moscovium",
            Element::Lv => "Livermorium",
            Element::Ts => "Tennessine",
            Element::Og => "Oganesson",
        }
    }
}
