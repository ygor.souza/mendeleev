[package]
name = "mendeleev"
version = "0.8.3"
edition = "2021"
rust-version = "1.62"
license = "MIT OR Apache-2.0"
authors = ["Ygor Souza <ygor.souza@protonmail.com>"]
description = "List of chemical elements, their isotopes, and their properties"
categories = ["science", "no-std", "no-std::no-alloc"]
keywords = ["periodic-table", "chemistry", "isotope", "atom", "atomic"]
readme = "README.md"
homepage = "https://gitlab.com/ygor.souza/mendeleev"
repository = "https://gitlab.com/ygor.souza/mendeleev"

# See more keys and their definitions at https://doc.rust-lang.org/cargo/reference/manifest.html

[dependencies]
serde = { version = "1.0", optional = true, features = ["derive"] }

[features]
default = ["all_features"]

all_features = ["all_no_std_features", "std"]
all_no_std_features = ["all_properties", "all_lists", "ranges"]
std = ["serde?/std"]
ranges = []

all_lists = ["element_list", "isotope_list", "group_list"]
element_list = []
group_list = []
isotope_list = ["isotopes"]

all_properties = ["all_element_properties", "all_isotope_properties"]

all_element_properties = ["numeric_properties", "string_properties", "color_properties", "electronic_configuration", "oxidation_states"]

electronic_configuration = []
oxidation_states = []

numeric_properties = ["atomic_number", "atomic_radius", "atomic_weight", "group_number", "period", "density", "electron_affinity", "ionization_energy", "year_discovered", "isotope_natural_abundance", "state_of_matter_properties"]
atomic_number = []
atomic_radius = []
atomic_weight = []
group_number = ["group"]
year_discovered = []
group = []
period = []
density = []
electron_affinity = []
ionization_energy = []

state_of_matter_properties = ["melting_point", "boiling_point", "fusion_heat", "evaporation_heat"]
melting_point = []
boiling_point = []
fusion_heat = []
evaporation_heat = []

color_properties = ["cpk_color", "jmol_color"]
cpk_color = ["color"]
jmol_color = ["color"]
color = []

string_properties = ["group_name", "group_symbol", "symbol", "name", "discoverers", "discovery_location"]
group_name = ["group"]
group_symbol = ["group"]
name = []
symbol = []
discoverers = []
discovery_location = []

all_isotope_properties = ["isotope_element", "isotope_mass_number", "isotope_natural_abundance", "isotope_neutron_number", "isotope_half_life", "isotope_display"]
isotope_element = ["isotopes"]
isotope_natural_abundance = ["isotopes"]
isotope_mass_number = ["isotopes"]
isotope_neutron_number = ["atomic_number", "isotope_mass_number", "isotope_element"]
isotope_half_life = ["isotopes"]
isotope_display = ["isotopes", "isotope_mass_number", "name", "symbol"]
isotopes = []


[[example]]
name = "print_all_elements"
required-features = ["std", "all_element_properties", "element_list"]

[[example]]
name = "print_periodic_table"
required-features = ["std", "symbol", "group", "period", "element_list", "group_list"]

[[example]]
name = "print_isotope_abundance"
required-features = ["std", "name", "isotope_natural_abundance", "isotope_display", "isotope_list", "isotope_element"]

[[example]]
name = "print_electronic_configuration"
required-features = ["std", "symbol", "electronic_configuration"]
